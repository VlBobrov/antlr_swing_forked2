tree grammar TExpr1;

options {
	tokenVocab=Expr;
	
	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}



@header {
package tb.antlr.interpreter;
}

prog: (print | dec)?;


errCatcher returns[Integer out]
  :
  expr{$out=$expr.out;}
  ;
    catch[RuntimeException ex]{System.err.println(ex); $out=0;}
expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(MOD e1=expr e2=expr) {}
        | ^(POW e1=expr e2=expr) {$out= (int) Math.pow($e1.out,$e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out,$e2.out);}
        | ^(PODST i1=ID   e2=expr)
        | ^(AND e1=expr e2=expr) {$out = $e1.out & $e2.out;}
        | ^(OR e1=expr e2=expr) {$out = $e1.out | $e2.out;}
        | INT                      {$out = getInt($INT.text);}
        | ID
        ;
 print :
      ^(PRINT e=expr){  drukuj ($e.text + " = " + $e.out.toString());}
      ;
dec: ^(VAR i1=ID){createVariable($i1.text);}
    ;