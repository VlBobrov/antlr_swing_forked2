grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}


prog
    : block EOF!
    ;

block
    :stat*
    ;

stat 
    : expr NL -> expr
    | VAR ID NL -> ^(VAR ID)
    //| ID PODST expr NL -> ^(PODST ID expr)
    | if_stat NL -> if_stat
    |  PRINT expr  NL -> ^( PRINT expr )
    | NL ->
    | OTHER {System.err.println("unknown char: " + $OTHER.text);}
    ;


if_stat
    : IF^ condition_block stat_block (ELSE! (stat_block))?
    ;     

condition_block
    : expr 
    ;
    
stat_block  
    : (OBRACE! NL!| OBRACE!) block CBRACE!
    | stat
    ; 


expr
    :multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : powExpr
      ( MUL^ powExpr
      | DIV^ powExpr
      )*
    ;
powExpr:
  atom (POW^ atom)?
  ;
atom
    : INT
    | (TRUE | FALSE) 
    | ID
    | LP! expr RP!
    | ID PODST expr NL -> ^(PODST ID expr)
    ;

PRINT : 'print';
EQ : '==';
NEQ : '!=';
TRUE: 'true';
FALSE: 'false';
IF : 'if';
ELSE: (NL | WS)* 'else';
OBRACE : '{';
CBRACE : '}';
NOT: '!';

VAR :'var';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP
  : '('
  ;

RP
  : ')'
  ;

PODST
  : '='
  ;

PLUS
  : '+'
  ;

MINUS
  : '-'
  ;

MUL
  : '*'
  ;

DIV
  : '/'
  ;
POW
  : '\^'
  ;
  
AND
  :'&'
  ;
OR
  :'|'
  ;
OTHER
  : .
  ;